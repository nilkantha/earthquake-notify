package np.com.geodata.database;

import np.com.geodata.Config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLDatabase implements Database {

	public MySQLDatabase() {
		initialize();
	}

	@Override
	public void initialize() {
		new MySQLDbCreator().create(this);
	}

	private Connection connect = null;

	@Override
	public void createConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			this.connect = DriverManager.getConnection("jdbc:mysql://" + Config.get("mysql", "host") + "/" + Config.get("mysql", "database")
					+ "?user=" + Config.get("mysql", "user") + "&password=" + Config.get("mysql", "password"));
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

	@Override
	public Connection getConnection() {
		return this.connect;
	}

	@Override
	public void commit() {
		if (null != this.connect) {
			try {
				this.connect.commit();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void closeConnection() {
		if (null != this.connect) {
			try {
				this.connect.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

}
