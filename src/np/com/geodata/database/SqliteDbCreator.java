package np.com.geodata.database;

import java.sql.SQLException;
import java.sql.Statement;

public class SqliteDbCreator implements DatabaseCreator {

	@Override
	public void create(Database db) {

		//Create a connection
		db.createConnection();
		//Create table to hold information
		createTables(db);
		//Finally close the connection
	}

	/**
	 * Function to create default table in the database
	 * 
	 * @param db
	 * @return void
	 */
	private void createTables(Database db) {

		String SQL = "CREATE TABLE IF NOT EXISTS `earthquakes` ( "
				+ "`uid` INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ "`id`	TEXT, " 
				+ "`created` INTEGER, " 
				+ "`time` TEXT, " 
				+ "`latitude` TEXT, "
				+ "`longitude` TEXT, " 
				+ "`depth` TEXT, " 
				+ "`magnitude` TEXT, " 
				+ "`magnitudeType` TEXT, "
				+ "`nst` TEXT, " 
				+ "`gap` TEXT, " 
				+ "`dmin` TEXT, "
				+ "`rms` TEXT, " 
				+ "`net` TEXT, " 
				+ "`updated` TEXT, " 
				+ "`place` TEXT, "
				+ "`type` TEXT,  " 
				+ "`horizontalError` TEXT, " 
				+ "`depthError` TEXT, " 
				+ "`magError` TEXT, " 
				+ "`magNst` TEXT, "
				+  "`status` TEXT, " 
				+ "`locationSource` TEXT, " 
				+ "`magSource` TEXT "
				+ ");";
		
		try {
			Statement stmt = db.getConnection().createStatement();
			
			stmt.executeUpdate(SQL);
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
