package np.com.geodata.database;

public interface DatabaseCreator {
	public void create(Database db);
}
