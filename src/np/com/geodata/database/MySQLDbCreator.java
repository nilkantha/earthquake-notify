package np.com.geodata.database;

import java.sql.SQLException;
import java.sql.Statement;

public class MySQLDbCreator implements DatabaseCreator {

	@Override
	public void create(Database db) {

		// Create a connection
		db.createConnection();
		// Create table to hold information
		createTables(db);
	}

	/**
	 * Function to create default table in the database
	 * 
	 * @param db
	 * @return void
	 */
	private void createTables(Database db) {

		String SQL = "CREATE TABLE IF NOT EXISTS `earthquakes` ("
				+ "`uid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, " 
				+ "`id` varchar(255) NULL, "
				+ " `created` int(11) NULL, " 
				+ "`time` varchar(255)  NULL, " 
				+ "`latitude` varchar(255)  NULL, "
				+ "`longitude` varchar(255)  NULL, " 
				+ "`depth` varchar(255)  NULL, "
				+ "`magnitude` varchar(255)  NULL, " 
				+ "`magnitudeType` varchar(255)  NULL, "
				+ "`nst` varchar(255)  NULL, " 
				+ "`gap` varchar(255)  NULL, " 
				+ "`dmin` varchar(255)  NULL, "
				+ "`rms` varchar(255)  NULL, " 
				+ "`net` varchar(255)  NULL, " 
				+ "`updated` varchar(255)  NULL, "
				+ "`place` varchar(255)  NULL, " 
				+ "`type` varchar(255)  NULL, "
				+ "`horizontalError` varchar(255)  NULL, " 
				+ "`depthError` varchar(255)  NULL, "
				+ "`magError` varchar(255)  NULL, " 
				+ "`magNst` varchar(255)  NULL, " 
				+ "`status` varchar(255)  NULL, "
				+ "`locationSource` varchar(255) NULL, " 
				+ "`magSource` varchar(255) NULL, " 
				+ " PRIMARY KEY (`uid`)"
				+ ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

		try {
			Statement stmt = db.getConnection().createStatement();

			stmt.executeUpdate(SQL);
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
