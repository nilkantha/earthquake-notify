package np.com.geodata.database;

import java.sql.Connection;

/**
 * Interface for database
 *
 * @author prabin
 */
public interface Database {

	public void initialize();

	public void createConnection();

	public Connection getConnection();

	public void commit();

	public void closeConnection();

}
