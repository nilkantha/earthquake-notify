package np.com.geodata.database;

import np.com.geodata.object.DataObject;

import java.util.List;

/**
 * Inerface for a general repository
 *
 * @param <T> Object of parent DataObject type
 *            
 * @author prabin
 */
public interface Repository<T extends DataObject> {
	public void add(T obj);

	public void addOrUpdate(T obj);

	public void remove(T obj);

	public T findByUid(long uid);

	public T findById(String id);

	public List<T> findAll();

	public T update(T obj);

}
