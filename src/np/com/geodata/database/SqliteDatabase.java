package np.com.geodata.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import np.com.geodata.Config;

/**
 * Class for handling sqlite database connection.
 *
 * @author prabin
 */
public class SqliteDatabase implements Database {

	private static Connection c = null;
	private String jdbc = "jdbc:sqlite:./" + Config.get("sqlite", "database-file");

	public SqliteDatabase() {
		initialize();
	}

	@Override
	public void initialize() {
		new SqliteDbCreator().create(this);
	}

	/**
	 * Create a database conection by calling DriverManager from jdbc
	 *
	 * @param void
	 * @return void
	 */
	public void createConnection() {
		if (c == null) {
			try {

				Class.forName("org.sqlite.JDBC");
				c = DriverManager.getConnection(jdbc);

			} catch (Exception e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				System.exit(0);
			}
		}
	}

	/**
	 * Commits changes to the databse
	 */
	public void commit() {
		try {
			c.commit();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			e.printStackTrace();
		}

	}

	/**
	 * Closes the database connnection.
	 */
	public void closeConnection() {
		try {
			c.close();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Returns the Connection object
	 *
	 * @return c Connection object
	 */
	public Connection getConnection() {
		return c;
	}
}
