package np.com.geodata.database;

import np.com.geodata.object.GeoDataObject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of Repository for GeoDataObjects. Has methods like add,
 * update, remove etc. Uses Database interface to perform CRUD operations.
 *
 * @author prabin
 */
public class GeoDataRepository implements Repository<GeoDataObject> {
	private Database db = null;
	private String table = "earthquakes";

	/**
	 * Constructor
	 *
	 * @param database
	 *            Database to store to.
	 */
	public GeoDataRepository(Database database) {
		db = database;
		db.createConnection();
	}

	/**
	 * Add GeoDataObject to the repository. Creates appropriate SQL statement to
	 * insert into the database specified by private db variable.
	 *
	 * @param obj
	 *            GeoDataObject
	 * @return void
	 */
	@Override
	public void add(GeoDataObject obj) {
		String SQL = "INSERT INTO `" + table + "` (`id`,`created`,`time`,`latitude`,`longitude`,`depth`,"
				+ "`magnitude`,`magnitudeType`, `nst`, `gap`,`dmin`,`rms`,`net`,`updated`,`place`, `type`, "
				+ "`horizontalError`, `depthError`, `magError`, `magNst`, `status`, `locationSource`, `magSource`) "
				+ " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,? ,?);";

		long unixTime = System.currentTimeMillis() / 1000L;

		try {
			PreparedStatement stmt = db.getConnection().prepareStatement(SQL);

			stmt.setString(1, obj.getId());
			stmt.setLong(2, unixTime);

			stmt.setString(3, obj.getTime());
			stmt.setString(4, obj.getLatitude());
			stmt.setString(5, obj.getLongitude());
			stmt.setString(6, obj.getDepth());
			stmt.setString(7, obj.getMagnitude());
			stmt.setString(8, obj.getMagnitudeType());
			stmt.setString(9, obj.getNst());
			stmt.setString(10, obj.getGap());
			stmt.setString(11, obj.getDmin());
			stmt.setString(12, obj.getRms());
			stmt.setString(13, obj.getNet());
			stmt.setString(14, obj.getUpdated());
			stmt.setString(15, obj.getPlace());
			stmt.setString(16, obj.getType());
			stmt.setString(17, obj.getHorizontalError());
			stmt.setString(18, obj.getDepthError());
			stmt.setString(19, obj.getMagError());
			stmt.setString(20, obj.getMagNst());
			stmt.setString(21, obj.getStatus());
			stmt.setString(22, obj.getLocationSource());
			stmt.setString(23, obj.getMagSource());

			stmt.executeUpdate();

			stmt.close();
			// db.commit();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Removes GeoDataObject from the database
	 *
	 * @param obj
	 *            GeoDataObject
	 * @return void
	 */
	@Override
	public void remove(GeoDataObject obj) {
		String SQL = "DELETE FROM `" + table + "` WHERE `uid` = ? OR `id` = ?;";

		try {
			PreparedStatement stmt = db.getConnection().prepareStatement(SQL);

			stmt.setLong(1, obj.getUid());
			stmt.setString(2, obj.getId());

			// db.commit();
			stmt.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Finds GeoDataObject by uid from the database
	 *
	 * @param uid long
	 * @return obj GeoDataObject
	 */
	@Override
	public GeoDataObject findByUid(long uid) {
		String SQL = "SELECT * FROM `" + table + "` WHERE `uid` = ?;";
		GeoDataObject obj = null;
		try {
			PreparedStatement stmt = db.getConnection().prepareStatement(SQL);

			stmt.setLong(1, uid);

			ResultSet rs = stmt.executeQuery();

			obj = new GeoDataObject();

			while (rs.next()) {
				obj.setUid(rs.getLong("uid"));
				obj.setId(rs.getString("id"));
				obj.setCreated(rs.getLong("created"));
				obj.setTime(rs.getString("time"));
				obj.setLatitude(rs.getString("latitude"));
				obj.setLongitude(rs.getString("longitude"));
				obj.setDepth(rs.getString("depth"));
				obj.setMagnitude(rs.getString("magnitude"));
				obj.setMagnitudeType(rs.getString("magnitudeType"));
				obj.setNst(rs.getString("nst"));
				obj.setGap(rs.getString("gap"));
				obj.setDmin(rs.getString("dmin"));
				obj.setRms(rs.getString("rms"));
				obj.setNet(rs.getString("net"));
				obj.setUpdated(rs.getString("updated"));
				obj.setPlace(rs.getString("place"));
				obj.setType(rs.getString("type"));
				obj.setHorizontalError(rs.getString("horizontalError"));
				obj.setDepthError(rs.getString("depthError"));
				obj.setMagError(rs.getString("magError"));
				obj.setMagNst(rs.getString("magNst"));
				obj.setStatus(rs.getString("status"));
				obj.setLocationSource(rs.getString("locationSource"));
				obj.setMagSource(rs.getString("magSource"));
				
			}
			rs.close();
			stmt.close();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return obj;
	}

	/**
	 * Finds GeoDataObject from the database
	 *
	 * @param id
	 *            String
	 * @return obj GeoDataObject
	 */
	@Override
	public GeoDataObject findById(String id) {
		String SQL = "SELECT * FROM `" + table + "` WHERE `id` = ?;";
		GeoDataObject obj = null;

		try {
			PreparedStatement stmt = db.getConnection().prepareStatement(SQL);

			stmt.setString(1, id);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				obj = new GeoDataObject();

				obj.setUid(rs.getLong("uid"));
				obj.setId(rs.getString("id"));
				obj.setCreated(rs.getLong("created"));
				obj.setTime(rs.getString("time"));
				obj.setLatitude(rs.getString("latitude"));
				obj.setLongitude(rs.getString("longitude"));
				obj.setDepth(rs.getString("depth"));
				obj.setMagnitude(rs.getString("magnitude"));
				obj.setMagnitudeType(rs.getString("magnitudeType"));
				obj.setNst(rs.getString("nst"));
				obj.setGap(rs.getString("gap"));
				obj.setDmin(rs.getString("dmin"));
				obj.setRms(rs.getString("rms"));
				obj.setNet(rs.getString("net"));
				obj.setUpdated(rs.getString("updated"));
				obj.setPlace(rs.getString("place"));
				obj.setType(rs.getString("type"));
				obj.setHorizontalError(rs.getString("horizontalError"));
				obj.setDepthError(rs.getString("depthError"));
				obj.setMagError(rs.getString("magError"));
				obj.setMagNst(rs.getString("magNst"));
				obj.setStatus(rs.getString("status"));
				obj.setLocationSource(rs.getString("locationSource"));
				obj.setMagSource(rs.getString("magSource"));

			}

			rs.close();
			stmt.close();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return obj;
	}

	/**
	 * Finds all GeoDataObject objects and returns a list of them
	 *
	 * @return List<GeoDataObject>
	 */
	@Override
	public List<GeoDataObject> findAll() {
		String SQL = "SELECT * FROM `" + table + "`;";
		GeoDataObject obj = null;

		List<GeoDataObject> objs = new ArrayList<GeoDataObject>();

		try {
			PreparedStatement stmt = db.getConnection().prepareStatement(SQL);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				obj = new GeoDataObject();

				obj.setUid(rs.getLong("uid"));
				obj.setId(rs.getString("id"));
				obj.setCreated(rs.getLong("created"));
				obj.setTime(rs.getString("time"));
				obj.setLatitude(rs.getString("latitude"));
				obj.setLongitude(rs.getString("longitude"));
				obj.setDepth(rs.getString("depth"));
				obj.setMagnitude(rs.getString("magnitude"));
				obj.setMagnitudeType(rs.getString("magnitudeType"));
				obj.setGap(rs.getString("gap"));
				obj.setDmin(rs.getString("dmin"));
				obj.setRms(rs.getString("rms"));
				obj.setNet(rs.getString("net"));
				obj.setUpdated(rs.getString("updated"));
				obj.setPlace(rs.getString("place"));
				obj.setNst(rs.getString("nst"));
				obj.setType(rs.getString("type"));
				obj.setHorizontalError(rs.getString("horizontalError"));
				obj.setDepthError(rs.getString("depthError"));
				obj.setMagError(rs.getString("magError"));
				obj.setMagNst(rs.getString("magNst"));
				obj.setStatus(rs.getString("status"));
				obj.setLocationSource(rs.getString("locationSource"));
				obj.setMagSource(rs.getString("magSource"));

				objs.add(obj);
			}
			rs.close();
			stmt.close();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return objs;
	}

	/**
	 * Updates the database with specific GeoDataObject
	 *
	 * @param obj GeoDataObject
	 * @return obj GeoDataObject
	 */
	@Override
	public GeoDataObject update(GeoDataObject obj) {
		String SQL = "UPDATE `" + table + "` SET " + "`time` = ?, `latitude` = ?, `longitude` = ?, `depth` = ?, "
				+ "`magnitude` = ?, `magnitudeType` = ?, `nst` = ?, "
				+ "`gap` = ?, `dmin` = ?, `rms` = ?, `net` = ?, `updated` = ?,"
				+ "`place` = ?, `type` = ?, `horizontalError` = ?, `depthError` = ?,`magError` = ?, `magNst` = ?, `status` = ?, `locationSource` = ?, `magSource` = ?  "
				+ " WHERE `id` = ? ;";
		try {
			PreparedStatement stmt = db.getConnection().prepareStatement(SQL);

			stmt.setString(1, obj.getTime());
			stmt.setString(2, obj.getLatitude());
			stmt.setString(3, obj.getLongitude());
			stmt.setString(4, obj.getDepth());
			stmt.setString(5, obj.getMagnitude());
			stmt.setString(6, obj.getMagnitudeType());
			stmt.setString(7, obj.getNst());
			stmt.setString(8, obj.getGap());
			stmt.setString(9, obj.getDmin());
			stmt.setString(10, obj.getRms());
			stmt.setString(11, obj.getNet());
			stmt.setString(12, obj.getUpdated());
			stmt.setString(13, obj.getPlace());
			stmt.setString(14, obj.getType());
			stmt.setString(15, obj.getHorizontalError());
			stmt.setString(16, obj.getDepthError());
			stmt.setString(17, obj.getMagError());
			stmt.setString(18, obj.getMagNst());
			stmt.setString(19, obj.getStatus());
			stmt.setString(20, obj.getLocationSource());
			stmt.setString(21, obj.getMagSource());

			stmt.setString(22, obj.getId());

			stmt.executeUpdate();

			// db.commit();
			stmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return obj;
	}

	/**
	 * Checks the specific object in the repository and performs add or update
	 * operation accordingly. Searches in the repository for record with
	 * specific id. If found then updates the record otherwise adds a new
	 * reconrd
	 *
	 * @param obj
	 *            GeoDataObject
	 * @return void
	 */
	@Override
	public void addOrUpdate(GeoDataObject obj) {

		GeoDataObject objOnRepo = findById(obj.getId());

		if (objOnRepo == null) {
			System.out.println("Added an object ");
			add(obj);
		} else {
			System.out.println("Found an object ");
			update(obj);
		}

	}

	/**
	 * Closes the database connection
	 *
	 * @return void
	 */

	public void close() {
		// db.commit();
		db.closeConnection();
	}

}
