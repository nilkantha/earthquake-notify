package np.com.geodata.sender;

/**
 * Interface for implementing Senders
 *
 * @author prabin
 */
public interface BasicSender {

    public void connect();

    public void send(String message);

    public void disconnect();

}
