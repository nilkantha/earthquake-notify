package np.com.geodata.sender;

import np.com.geodata.Config;
import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;

import java.io.IOException;

/**
 * Class to send XMPP messages to specified user
 *
 * @author prabin
 */
public class XMPPSender implements BasicSender {

	private XMPPTCPConnectionConfiguration.Builder configBuilder = null;
	private AbstractXMPPConnection connection = null;

	private String userName = null;
	private String passWord = null;
	private String xmppTo = null;

	/**
	 * Constructor
	 *
	 * @param userName
	 *            String for username
	 * @param passWord
	 *            String for password
	 * @param xmppTo
	 *            String for user's XMPP endpoint.
	 */
	public XMPPSender(String userName, String passWord, String xmppTo) {
		this.userName = userName;
		this.passWord = passWord;
		this.xmppTo = xmppTo;
	}

	/**
	 * function to connect to XMPP Server
	 */
	@Override
	public void connect() {
		configBuilder = XMPPTCPConnectionConfiguration.builder();

		configBuilder.setUsernameAndPassword(this.userName, this.passWord);
		configBuilder.setHost(Config.get("xmpp", "server"));
		configBuilder.setPort(5222);
		configBuilder.setSecurityMode(SecurityMode.ifpossible);
		configBuilder.setServiceName(Config.get("xmpp", "server"));
		configBuilder.setEnabledSSLProtocols(new String[] {"SSLv3", "TLSv1", "TLSv1.2", "SSLv2Hello", "TLSv1.1",});

		SmackConfiguration.DEBUG = true;

		this.connection = new XMPPTCPConnection(configBuilder.build());

		try {
			this.connection.setPacketReplyTimeout(10000);
			this.connection.connect();
			this.connection.login();
		} catch (SmackException | IOException | XMPPException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Send message specified by the message variable
	 *
	 * @param message
	 *            String message to send
	 */
	@Override
	public void send(String message) {
		Message msg = new Message(this.xmppTo, message);
		try {
			connection.sendStanza(msg);
		} catch (NotConnectedException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Disconnects XMPP connection
	 */
	@Override
	public void disconnect() {
		if (null != this.connection)
			this.connection.disconnect();

	}

}
