package np.com.geodata;

import np.com.geodata.database.Repository;
import np.com.geodata.object.GeoDataObject;
import np.com.geodata.sender.BasicSender;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to insert new GeoDataObject list got from USGS's URL to sqlite database
 * and send a preformatted message to user's XMPP endpoint.
 *
 * @author prabin
 */
public class Analyzer {

	private Repository<GeoDataObject> repo = null;
	private BasicSender sender = null;

	/**
	 * Constructor. Requires BasicSender Object and GeoDataObject Repository
	 *
	 * @param sender
	 * @param repo
	 */
	public Analyzer(BasicSender sender, Repository<GeoDataObject> repo) {
		this.sender = sender;
		this.repo = repo;
	}

	/**
	 * Analyzes the GeoDataObject list. If there is no record of the object in
	 * the database the function stores the object in the sqlite database and
	 * sends XMPP message to the specified user.
	 *
	 * @param objects
	 * @return void
	 */
	public void analyze(List<GeoDataObject> objects) {
		
		if (objects.size() > 0) {

			List<String> messageString = new ArrayList<String>();

			for (GeoDataObject obj : objects) {
			
				if (repo.findById(obj.getId()) == null) {
					// Generate Message String
					if (Float.parseFloat(obj.getMagnitude()) > Float.parseFloat(Config.get("xmpp", "send-magnitude-greater-than"))) {
						DateTime dtHere = parseISODate(obj.getTime());
						String dated = dtHere.toString("d MMMM, yyyy, hh:mm a");
						String tempStr = "Earthquake on " + dated + " near " + obj.getPlace() + " with a magnitude of "
								+ obj.getMagnitude();
						messageString.add(tempStr);
					}
					// Store the object in the database
					repo.add(obj);
				} else {
					repo.update(obj);
				}
			}

			boolean sendMessages = Boolean.parseBoolean(Config.get("config", "send-message"));
			
			if (sendMessages) {
				int numberOfMessages = messageString.size();
				if (numberOfMessages > 0) {
					sender.connect();
					for (String s : messageString) {
						System.out.println(s);
						sender.send(s);
	
					}
					sender.disconnect();
				}
			}
		}

	}

	/**
	 * Parses ISO date to DateTime format
	 *
	 * @param isoDate String
	 * @return DateTime Object
	 */
	private DateTime parseISODate(String isoDate) {
		DateTimeFormatter parser = ISODateTimeFormat.dateTimeParser();
		// DateTimeFormatter formatter = ISODateTimeFormat.dateTimeNoMillis();

		DateTime dateTimehere = parser.parseDateTime(isoDate);
		// DateTime dateTimeInNepal =
		// dateTimehere.withZone(DateTimeZone.forID("Asia/Nepal"));

		return dateTimehere;
	}
}
