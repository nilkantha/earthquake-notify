package np.com.geodata;

import java.io.File;
import java.io.IOException;
import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;

/**
 * Configuration class. Contains static strings for XMPP connection and USGS's
 * CSV URI
 *
 * @author prabin
 */
public class Config {

	public static String get(String header, String key) {
		
		try {
			Ini ini = new Ini(new File("config.ini"));
			return ini.get(header, key);
			
		} catch (InvalidFileFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}

}
