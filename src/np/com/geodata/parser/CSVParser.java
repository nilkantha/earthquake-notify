package np.com.geodata.parser;

import com.opencsv.CSVReader;
import np.com.geodata.object.GeoDataObject;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Parses CSV data received from USGS's website and prepares a list of
 * GeoDataObjects and returns it.
 *
 * @author prabin
 */
public class CSVParser implements Parser<GeoDataObject> {
	private CSVReader reader = null;

	/**
	 * Constructor
	 *
	 * @param string
	 *            Comma separated string.
	 */
	public CSVParser(String string) {
		reader = new CSVReader(new StringReader(string), ',', '"', 1);
	}

	/**
	 * This function extracts the data from CSV string. It loops through every
	 * line of the string, makes an object of every line of CSV with appropriate
	 * information and adds to a list
	 *
	 * @return List<GeoDataObject> List of GeoDataObject
	 */
	@Override
	public List<GeoDataObject> parse() {

		List<GeoDataObject> objs = new ArrayList<GeoDataObject>();

		try {
			List<String[]> myEntries = reader.readAll();

			for (String[] str : myEntries) {
				GeoDataObject obj = new GeoDataObject();

				obj.setTime(str[0]);
				obj.setLatitude(str[1]);
				obj.setLongitude(str[2]);
				obj.setDepth(str[3]);
				obj.setMagnitude(str[4]);
				obj.setMagnitudeType(str[5]);
				obj.setNst(str[6]);
				obj.setGap(str[7]);
				obj.setDmin(str[8]);
				obj.setRms(str[9]);
				obj.setNet(str[10]);
				obj.setId(str[11]);
				obj.setUpdated(str[12]);
				obj.setPlace(str[13]);
				obj.setType(str[14]);
				obj.setHorizontalError(str[15]);
				obj.setDepthError(str[16]);
				obj.setMagError(str[17]);
				obj.setMagNst(str[18]);
				obj.setStatus(str[19]);
				obj.setLocationSource(str[20]);
				obj.setMagSource(str[21]);

				objs.add(obj);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return objs;
	}

}
