package np.com.geodata.parser;

import np.com.geodata.object.DataObject;

import java.util.List;

/**
 * Parser Interface
 *
 * @param <T> Object of parent type DataObject
 * @author prabin
 */
public interface Parser<T extends DataObject> {

    public List<T> parse();

}
