package np.com.geodata;

import np.com.geodata.database.Database;
import np.com.geodata.database.GeoDataRepository;
import np.com.geodata.database.MySQLDatabase;
import np.com.geodata.database.Repository;
import np.com.geodata.database.SqliteDatabase;
import np.com.geodata.object.GeoDataObject;
import np.com.geodata.parser.CSVParser;
import np.com.geodata.parser.Parser;
import np.com.geodata.sender.BasicSender;
import np.com.geodata.sender.XMPPSender;

import java.util.List;

/**
 * Main class for the program
 *
 * @author prabin
 */
public class GeoData {

	/**
	 * Main function
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		
		HTTPGet getter = new HTTPGet(Config.get("config", "url"));
		String response = getter.getResponseString();

		Database db = null;
		
		boolean useMysql = Boolean.parseBoolean(Config.get("config","use-mysql"));
		if (useMysql) {
			db = new MySQLDatabase();
		} else {
			db = new SqliteDatabase();
		}

		Repository<GeoDataObject> repo = new GeoDataRepository(db);
		Parser<GeoDataObject> parser = new CSVParser(response);
		List<GeoDataObject> objectList = parser.parse();

		BasicSender sender = new XMPPSender(Config.get("xmpp", "username"), Config.get("xmpp", "password"), Config.get("xmpp", "to"));
		Analyzer analyzer = new Analyzer(sender, repo);

		analyzer.analyze(objectList);
		db.closeConnection();
	}

}
