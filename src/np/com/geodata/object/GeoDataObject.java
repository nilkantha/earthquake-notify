package np.com.geodata.object;

/**
 * Implementation of a generic data object GeoDataObjects which maps every
 * attributes to every columns of CSV returned from USGS's website.
 *
 * @author prabin
 */
public class GeoDataObject extends DataObject {
	private long uid = 0;
	private long created = 0;
	private String time = null;
	private String latitude = null;
	private String longitude = null;
	private String depth = null;
	private String magnitude = null;
	private String magnitudeType = null;
	private String nst = null;
	private String gap = null;
	private String dmin = null;
	private String rms = null;
	private String net = null;
	private String id = null;
	private String updated = null;
	private String place = null;
	private String type = null;
	private String horizontalError = null;
	private String depthError = null;
	private String magError = null;
	private String magNst = null;
	private String status = null;
	private String locationSource = null;
	private String magSource = null;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getDepth() {
		return depth;
	}

	public void setDepth(String depth) {
		this.depth = depth;
	}

	public String getMagnitude() {
		return magnitude;
	}

	public void setMagnitude(String magnitude) {
		this.magnitude = magnitude;
	}

	public String getMagnitudeType() {
		return magnitudeType;
	}

	public void setMagnitudeType(String magnitudeType) {
		this.magnitudeType = magnitudeType;
	}

	public String getGap() {
		return gap;
	}

	public void setGap(String gap) {
		this.gap = gap;
	}

	public String getDmin() {
		return dmin;
	}

	public void setDmin(String dmin) {
		this.dmin = dmin;
	}

	public String getRms() {
		return rms;
	}

	public void setRms(String rms) {
		this.rms = rms;
	}

	public String getNet() {
		return net;
	}

	public void setNet(String net) {
		this.net = net;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUpdated() {
		return updated;
	}

	public void setUpdated(String updated) {
		this.updated = updated;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public String getNst() {
		return nst;
	}

	public void setNst(String nst) {
		this.nst = nst;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getHorizontalError() {
		return horizontalError;
	}

	public void setHorizontalError(String horizontalError) {
		this.horizontalError = horizontalError;
	}

	public String getDepthError() {
		return depthError;
	}

	public void setDepthError(String depthError) {
		this.depthError = depthError;
	}

	public String getMagError() {
		return magError;
	}

	public void setMagError(String magError) {
		this.magError = magError;
	}

	public String getMagNst() {
		return magNst;
	}

	public void setMagNst(String nagNst) {
		this.magNst = nagNst;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLocationSource() {
		return locationSource;
	}

	public void setLocationSource(String locationSource) {
		this.locationSource = locationSource;
	}

	public String getMagSource() {
		return magSource;
	}

	public void setMagSource(String magSource) {
		this.magSource = magSource;
	}

}
