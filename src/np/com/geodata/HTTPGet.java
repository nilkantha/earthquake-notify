package np.com.geodata;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Class to get data through HTTP
 *
 * @author prabin
 */
public class HTTPGet {

    private String url = null;

    /**
     * Constructor. Requires URL string
     *
     * @param url String
     */
    public HTTPGet(String url) {
        this.url = url;
    }

    /**
     * Creates HTTP client, Sends requests and receives
     * response from the URL.
     * Gets the response body and returns as a string.
     *
     * @return str Response string.
     */
    public String getResponseString() {
        
        CloseableHttpClient client = HttpClients.createDefault();
        HttpGet getVar = new HttpGet(url);

        String str = null;

        try {
            CloseableHttpResponse response = client.execute(getVar);
            HttpEntity entity = response.getEntity();
            str = entity != null ? EntityUtils.toString(entity) : null;
            EntityUtils.consume(entity);
            response.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

		return str;
	}
}
