# GeoData

## Abstract

The project is developed to fetch earthquake data from USGS's Earthquake Hazards Program.
There are many earthquakes happening around the world. Some are insignificant but others are hazardous. This project aims to fetch those data which have magnitude of more than 4.5 periodically and send a small message regarding the earthquake.The accuracy of the information purely depends on the data USGS provides. 

The project is written in Java Language with smack library and other dependencies. 

## How to run

* Clone the repository 
    git clone git@gitlab.com:nilkantha/earthquake-notify.git

* Open the repository in eclipse

* Look at the configuration class Config.java. Change the strings to your configuration.

* Run the project from Eclipse. 

* You can export project as runnable jar. Add crontab to run every 5 or 10 minutes.


# License

```
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
```